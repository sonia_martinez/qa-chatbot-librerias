import React from 'react'
//import { FormattedMessage } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
import {Chatbot} from './components/Chatbot';
// import './styles/index.css';
import './styles/estilosg.css';

const CSS_HANDLES = ['title']

const IndexChat: StorefrontFunctionComponent<IndexChatProps> = ({ titleChat}) => {
  const handles = useCssHandles(CSS_HANDLES)
 // const titleText = title || <FormattedMessage id="editor.countdown.title" />
 const titleChatText=titleChat||"PRUeBA CHAT"

  return (
    <div className={`${handles.title} t-heading-2 fw3 w-100 c-muted-1 db tc`}>
      {titleChatText}
      <Chatbot></Chatbot>
    </div>
  )
}

interface IndexChatProps {
  titleChat: string
}

IndexChat.schema = {
  titleChat: 'PRUEBA DE COMPONENTE CHAT',
  description: 'editor.countdown-title.description',
  type: 'object',
  properties: {
    title: {
      title: 'I am a title',
      type: 'string',
      default: null,
    },
  },
}

export default IndexChat